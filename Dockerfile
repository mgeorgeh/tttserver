FROM ibmcom/swift-ubuntu:3.1.1
ADD . /app
WORKDIR /app
RUN swift build --configuration release
EXPOSE 8080
ENTRYPOINT [".build/release/TTTServer"]