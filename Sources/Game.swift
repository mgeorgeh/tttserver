//
//  Game.swift
//  TTTServer
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import KituraWebSocket

extension Array {
  func all(_ cond: (Array.Element) -> Bool) -> Bool {
    for elt in self {
      if !cond(elt) {
        return false
      }
    }
    return true
  }
  
  func any(_ cond: (Array.Element) -> Bool) -> Bool {
    for elt in self {
      if cond(elt) {
        return true
      }
    }
    return false
  }
}

enum GameEndState {
  case win(token: String)
  case tie
}

//func ==(lhs: GameEndState, rhs: GameEndState) -> Bool {
//  switch (lhs, rhs) {
//  case (.tie, .tie):
//    return true
//  case (.win(let s1), .win(let s2)):
//    return s1 == s2
//  default:
//    return false
//  }
//}

class Game {
  var conn1: WebSocketConnection
  var conn2: WebSocketConnection
  var activePlayerID: String
  var activeToken: String
  var uuid: String
  var cells = [
    ["-", "-", "-"],
    ["-", "-", "-"],
    ["-", "-", "-"]
  ]
  
  var gameEnd: GameEndState?
  
  var mainDiag: [String] {
    return [cells[0][0], cells[1][1], cells[2][2]]
  }
  
  var offDiag: [String] {
    return [cells[0][2], cells[1][1], cells[2][0]]
  }
  
  init(conn1: WebSocketConnection, conn2: WebSocketConnection, activePlayerID: String) {
    self.conn1 = conn1
    self.conn2 = conn2
    self.activePlayerID = activePlayerID
    self.activeToken = "x"
    self.uuid = UUID().uuidString
    print("created game with id1: \(conn1.id), id2: \(conn2.id), game id: \(self.uuid)")
  }
  
  func isValidMove(_ x: Int, _ y: Int) -> Bool {
    return (0...2).contains(x) && (0...2).contains(y) && cells[2 - y][x] == "-"
  }
  
  func makeMove(_ x: Int, _ y: Int) {
    let (i, j) = (2 - y, x)
    cells[i][j] = activeToken
    
    var victories: [[String]] = [cells[i], cells.map({ $0[j] })]

    if i == j {
      victories.append(mainDiag)
    }
    
    if j == 2 - i {
      victories.append(offDiag)
    }
    
    if victories.any({ row in row.all({ $0 == activeToken })}) {
      gameEnd = .win(token: activeToken)
    } else if cells.all({row in row.all { $0 != "-" }}) {
      gameEnd = .tie
    } else {
      flipActivePlayer()
      flipActiveToken()
    }
  }
  
  func flipActivePlayer() {
    activePlayerID = (activePlayerID == conn1.id) ? conn2.id : conn1.id
  }
  
  func flipActiveToken() {
    activeToken = (activeToken == "o") ? "x" : "o"
  }
  

  
  
}
