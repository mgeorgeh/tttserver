
//
//  Responder.swift
//  TTTServer
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import KituraWebSocket

class Responder {
//  var openGames: [WebSocketConnection] = []
  var gameIDs: [String] {
    return Array<String>(connections.keys)
  }
  
  // games being played
  var currentGames: [String: Game] = [:]
  
  var connections: [String: WebSocketConnection] = [:]
  
  // do stuff the dumb way, then refactor if necessary
  func respondTo(_ msg: ClientMessage, conn: WebSocketConnection) -> Void {
    switch msg {
    case .createGame:
      guard connections[conn.id] == nil else {
        conn.sendMessage(.error)
        return
      }
      connections[conn.id] = conn
      conn.sendMessage(.gameCreateSuccess)
      
    case .getOpenGames:
      conn.sendMessage(.listOpenGames(games: gameIDs))
      
    case .joinGame(gameID: let id):
      guard let conn1 = connections[id], id != conn.id else {
        conn.sendMessage(.error)
        return
      }
      let newGame = Game(conn1: conn1, conn2: conn, activePlayerID: conn1.id)
      let gameuuid = newGame.uuid
      currentGames[gameuuid] = newGame
      conn.sendMessage(.gameStart(gameID: gameuuid, goingFirst: false))
      conn1.sendMessage(.gameStart(gameID: gameuuid, goingFirst: true))
    
    case .makeMove(gameID: let id, xcoord: let x, ycoord: let y):
      //validate. is it current player's turn, is player trying to move on the right game
      guard let game = currentGames[id], conn.id == game.activePlayerID, game.isValidMove(x, y) else {
        conn.sendMessage(.error)
        return
      }
      let token = game.activeToken
      game.makeMove(x, y)
      if let v = game.gameEnd {
        game.conn1.sendMessage(.gameEnd(state: v))
        game.conn2.sendMessage(.gameEnd(state: v))
      } else {
        game.conn1.sendMessage(.moveMade(tokenType: token, xcoord: x, ycoord: y))
        game.conn2.sendMessage(.moveMade(tokenType: token, xcoord: x, ycoord: y))
      }
      
      
    default:
      conn.sendMessage(.error)
    }
  }
}
