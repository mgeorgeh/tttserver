//
//  ServerResponse.swift
//  TTTClient
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import SwiftyJSON

enum ServerMessage {
  case gameCreateSuccess
  case gameStart(gameID: String, goingFirst: Bool)
  case listOpenGames(games: [String])
  case moveMade(tokenType: String, xcoord: Int, ycoord: Int)
  case gameEnd(state: GameEndState)
  case error
    
  func toData() -> Data {
    var result: JSON = [:]
    switch self {
    case .gameCreateSuccess:
      result = ["msgType": "gameCreateSuccess"]
      
    case .gameStart(gameID: let id, goingFirst: let b):
      result = [
        "msgType": "gameStart",
        "params": [
          "gameID": id,
          "goingFirst": b
        ]
      ]
      
    case .listOpenGames(games: let games):
      result = [
        "msgType": "listOpenGames",
        "params": [
          "games": games
        ]
      ]
      
    case .moveMade(tokenType: let token, xcoord: let x, ycoord: let y):
      result = [
        "msgType": "moveMade",
        "params": [
          "tokenType": token,
          "xcoord": x,
          "ycoord": y
        ]
      ]
      
    case .gameEnd(state: let s):
      var tokenStr = ""
      switch s {
      case .win(token: let s):
        tokenStr = s
      default:
        tokenStr = ""
      }
      
      result = [
        "msgType": "gameEnd",
        "params": [
          "tokenStr": tokenStr
        ]
      ]

      
    default:
      result = ["msgType": "error"]
    }
    return try! result.rawData()
  }
}
