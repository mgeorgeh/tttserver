//
//  WebSocketMessage.swift
//  TTTServer
//
//  Created by Charlemagne on 6/18/17.
//
//

import Foundation
import KituraWebSocket

extension WebSocketConnection {
  func sendMessage(_ msg: ServerMessage) -> Void {
    self.send(message: msg.toData())
  }
}
