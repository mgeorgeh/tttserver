//
//  ClientCommand.swift
//  TTTClient
//
//  Created by Charlemagne on 6/17/17.
//
//

import Foundation
import SwiftyJSON

enum ClientMessage {
  case createGame
  case getOpenGames
  case joinGame(gameID: String)
  case makeMove(gameID: String, xcoord: Int, ycoord: Int)
  case error
  
  init(data: Data) {
    let jsonData = JSON(data: data)
    
    guard let msgType = jsonData["msgType"].string else {
      self = .error
      return
    }
    
    switch msgType {
    case "createGame":
      self = .createGame
      
    case "getOpenGames":
      self = .getOpenGames
      
    case "joinGame":
      guard let gameID = jsonData["params"]["gameID"].string else {
        self = .error
        return
      }
      self = .joinGame(gameID: gameID)
    
    case "makeMove":
      guard let gameID = jsonData["params"]["gameID"].string,
        let xcoord = jsonData["params"]["xcoord"].int,
        let ycoord = jsonData["params"]["ycoord"].int else {
          self = .error
          return
      }
      self = .makeMove(gameID: gameID, xcoord: xcoord, ycoord: ycoord)
      
    default:
      self = .error
    }
  }
}
