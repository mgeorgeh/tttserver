import Foundation
import Kitura
import KituraWebSocket
import HeliumLogger
import LoggerAPI

import SwiftyJSON

HeliumLogger.use(.info)

class TTTService: WebSocketService {
  var responder = Responder()
  
  public func connected(connection: WebSocketConnection) {
    Log.info("connection initiated with id \(connection.id)")
  }
  
  public func disconnected(connection: WebSocketConnection, reason: WebSocketCloseReasonCode) {
    Log.info("connection terminated: \(connection.id)")
  }
  
  public func received(message: Data, from: WebSocketConnection) {
    let clientMessage = ClientMessage(data: message)
    responder.respondTo(clientMessage, conn: from)
  }
  
  public func received(message: String, from: WebSocketConnection) {
    
    Log.info("Got message \(message)... sending it back")
    from.send(message: message)
  }
}

//sendMessage(.joinGameSuccess, to: conn1)

// Create a new router
let router = Router()

WebSocket.register(service: TTTService(), onPath: "tictactoe")

let port = 8080

Kitura.addHTTPServer(onPort: port, with: router)

Kitura.run()
