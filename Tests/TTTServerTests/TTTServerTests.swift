import XCTest
@testable import TTTServer

class TTTServerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(TTTServer().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
